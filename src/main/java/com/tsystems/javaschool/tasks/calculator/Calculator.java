package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    static private byte GetPriority(char s)
    {
        switch (s)
        {
            case '(': return 0;
            case ')': return 1;
            case '+': return 2;
            case '-': return 3;
            case '*': return 4;
            case '/': return 4;
            default: return 6;
        }
    }

    public static String evaluate(String statement) {
        Stack stack = new Stack();
        String temp = new String();
        int parentheses = 0;

        if(statement == null || statement.length() == 0)
            return null;

        int i = 0;
        while(i < statement.length()){
            if(Character.isDigit(statement.charAt(i))){
                while(Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.'){
                    if(statement.charAt(i) == '.' && statement.charAt(i+1) == '.')
                        return null;
                    temp += statement.charAt(i);
                    i++;
                    if(i == statement.length()) break;
                }
                temp += " ";
            }
            else if(statement.charAt(i) == '('){
                parentheses++;
                stack.push(statement.charAt(i));
                i++;
            }
            else if(statement.charAt(i) == ')'){
                parentheses--;
                char s = (char) stack.pop();
                while (s != '('){
                    temp +=  s + " ";
                    s = (char) stack.pop();
                }
                i++;
            }
            else {
                if(statement.charAt(i) == statement.charAt(i+1))
                    return null;
                if (stack.isEmpty() == false) {
                    if (GetPriority(statement.charAt(i)) <= GetPriority((char) stack.peek())) {
                        temp += stack.pop().toString() + " ";
                    }
                }
                stack.push(statement.charAt(i));
                if(GetPriority(statement.charAt(i)) == 6)
                    return null;
                i++;
            }
        }

        while (stack.isEmpty() == false){
            temp += (char)stack.pop() + " ";
        }

        if (parentheses != 0)
            return null;

        double result = 0;

        for(i = 0; i < temp.length(); i++){

            if(Character.isDigit(temp.charAt(i))){
                String a = new String();
                while (temp.charAt(i) != ' ' && temp.charAt(i) != '/' && temp.charAt(i) != '*' && temp.charAt(i) != '-' && temp.charAt(i) != '+'){
                    a += temp.charAt(i);
                    i++;
                    if(i == temp.length()) break;
                }
                stack.push(Double.parseDouble(a));
                i--;
            }

            else if (temp.charAt(i) == '*' || temp.charAt(i) == '-' || temp.charAt(i) == '+' || temp.charAt(i) == '/'){
                double a = (double) stack.pop();
                double b = (double) stack.pop();

                switch (temp.charAt(i)){
                    case '+': result = b + a; break;
                    case '-': result = b - a; break;
                    case '*': result = b * a; break;
                    case '/': if(a == 0) return null; else result = b / a; break;
                }
                stack.push(result);
            }
        }

        if(result % 1 == 0)
            return Integer.toString((int)result);
        else {
            result *= 10000;
            Math.round(result);
            result /= 10000;
            return Double.toString(result);
        }
    }

}
