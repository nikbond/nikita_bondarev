package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int size = 0, height = 0, length = 1;

        if(inputNumbers.size() > 10000) throw new CannotBuildPyramidException();

        while (size < inputNumbers.size()){
            height++;
            size += height;
        }

        if(size != inputNumbers.size()) throw new CannotBuildPyramidException();

        for(int i = 0; i < inputNumbers.size(); i++){
            if(inputNumbers.get(i) == null)
                throw new CannotBuildPyramidException();
        }

        for(int i = 1; i < height; i++){
            length += 2;
        }

        for(int i = 0; i < inputNumbers.size(); i++){
            for(int j = i + 1; j < inputNumbers.size();j++){
                if(inputNumbers.get(i) > inputNumbers.get(j)){
                    int tempInt = inputNumbers.get(i);
                    inputNumbers.set(i, inputNumbers.get(j));
                    inputNumbers.set(j, tempInt);
                }
            }
        }

        int array[][] = new int[height][length];
        int middle = length/2, mostLeft = middle, mostRight = middle, k = 0;
        for(int i = 0; i < height; i++){
            for(int j = 0; j < length; j++){
                if(j == middle && mostLeft == mostRight){
                    array[i][middle] = inputNumbers.get(k);
                    k++;
                    mostLeft--;
                    mostRight++;
                }
                else if(j == mostLeft){
                    while (j <= mostRight){
                        array[i][j] = inputNumbers.get(k);
                        k++;
                        j += 2;
                    }
                    mostLeft--;
                    mostRight++;
                }
                else
                    array[i][j] = 0;
            }
        }
        return array;
    }
}
