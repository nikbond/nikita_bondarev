package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    public boolean find(List x, List y) throws IllegalArgumentException{
        if(x == null || y == null) throw new IllegalArgumentException();
        int found = 0, amount = 0;
        for (int i = 0; i < x.size(); i++){
            for (int j = found; j < y.size(); j++){
                if(x.get(i) == y.get(j)){
                    found = j;
                    amount++;
                    break;
                }
            }
        }
        if (amount == x.size())
            return true;
        else
            return false;
    }
}
